package com.example.projet;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import static android.database.sqlite.SQLiteDatabase.CONFLICT_IGNORE;

public class ObjectDbHelper extends SQLiteOpenHelper {

    private static final String TAG = ObjectDbHelper.class.getSimpleName();

    // If you change the database schema, you must increment the database version.
    private static final int DATABASE_VERSION = 1;

    public static final String DATABASE_NAME = "museum.db";

    public static final String TABLE_NAME = "MUSEUM";

    public static final String _ID = "_id";
    public static final String COLUMN_OBJECT_ID = "idObject";
    public static final String COLUMN_OBJECT_NAME = "object";
    public static final String COLUMN_BRAND = "brand";
    public static final String COLUMN_YEAR = "year";
    public static final String COLUMN_DECADE = "dacade";
    public static final String COLUMN_DESC = "descri";
    public static final String COLUMN_DETAILS = "details";
    public static final String COLUMN_CAT = "cat";
    public static final String COLUMN_IMG = "img";

    public Context mContext;
    public Activity activity;

    public ObjectDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.mContext = context;
    }

    public ObjectDbHelper(Context context, Activity activity) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.mContext = context;
        this.activity = activity;
    }


    @Override
    public void onCreate(SQLiteDatabase db) {

        final String SQL_CREATE_BOOK_TABLE = "CREATE TABLE " + TABLE_NAME + " (" +
            _ID + " INTEGER PRIMARY KEY," +
            COLUMN_OBJECT_ID + " TEXT, " +
            COLUMN_OBJECT_NAME + " TEXT, " +
            COLUMN_BRAND + " TEXT, " +
            COLUMN_YEAR + " TEXT, " +
            COLUMN_DECADE + " TEXT, " +
            COLUMN_DESC + " TEXT, " +
            COLUMN_DETAILS + " TEXT, " +
            COLUMN_CAT + " TEXT, " +
            COLUMN_IMG + " BLOB, " +
            " UNIQUE (" + COLUMN_OBJECT_ID  + ") ON CONFLICT ROLLBACK);";

        db.execSQL(SQL_CREATE_BOOK_TABLE);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    private ContentValues fill(Object object) {
        ContentValues values = new ContentValues();
        values.put(COLUMN_OBJECT_ID, object.getIdObject());
        values.put(COLUMN_OBJECT_NAME, object.getName());
        values.put(COLUMN_BRAND, object.getBrand());
        values.put(COLUMN_YEAR, object.getYear());
        values.put(COLUMN_DECADE, object.getDecade());
        values.put(COLUMN_DESC, object.getDesc());
        values.put(COLUMN_DETAILS, object.getDetails());
        values.put(COLUMN_CAT, object.getCat());
        values.put(COLUMN_IMG, object.getImg());
        return values;
    }


    public boolean addObject(Object object) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = fill(object);

        Log.d(TAG, "adding: "+object.getName()+" with id="+object.getId());

        long rowID = db.insertWithOnConflict(TABLE_NAME, null, values, CONFLICT_IGNORE);
        db.close();

        return (rowID != -1);
    }

    public Cursor fetchAllObject() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(TABLE_NAME, null,
                null, null, null, null, COLUMN_OBJECT_NAME +" ASC", null);

        Log.d(TAG, "call fetchAllTeams()");
        if (cursor != null) {
            cursor.moveToFirst();
        }
        return cursor;
    }


    public void populate() {
        Log.d(TAG, "call populate()");

        new APICommunicator(mContext, activity).execute();

        SQLiteDatabase db = this.getReadableDatabase();
        long numRows = DatabaseUtils.longForQuery(db, "SELECT COUNT(*) FROM "+TABLE_NAME, null);
        Log.d(TAG, "nb of rows="+numRows);
        db.close();
    }

    public static Object cursorToObject(Cursor cursor) {

        Object object = new Object(cursor.getLong(cursor.getColumnIndex(_ID)),
                cursor.getString(cursor.getColumnIndex(COLUMN_OBJECT_ID)),
                cursor.getString(cursor.getColumnIndex(COLUMN_OBJECT_NAME)),
                cursor.getString(cursor.getColumnIndex(COLUMN_BRAND)),
                cursor.getString(cursor.getColumnIndex(COLUMN_YEAR)),
                cursor.getString(cursor.getColumnIndex(COLUMN_DECADE)),
                cursor.getString(cursor.getColumnIndex(COLUMN_DESC)),
                cursor.getString(cursor.getColumnIndex(COLUMN_DETAILS)),
                cursor.getString(cursor.getColumnIndex(COLUMN_CAT)),
                cursor.getBlob(cursor.getColumnIndex(COLUMN_IMG))
        );

        return object;
    }
}