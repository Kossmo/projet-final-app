package com.example.projet;

import android.util.JsonReader;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class JSONResponseHandlerObject {

    private static final String TAG = JSONResponseHandlerObject.class.getSimpleName();

    private ArrayList<Object> object;


    public JSONResponseHandlerObject() {
        this.object = new ArrayList<>();
    }

    public ArrayList<Object> getObject(){ return this.object;}


    public void readJsonStream(InputStream response, String idO) throws IOException {
        JsonReader reader = new JsonReader(new InputStreamReader(response, "UTF-8"));
        try {
            readObjects(reader,idO);
        } finally {
            reader.close();
        }
    }

    public void readObjects(JsonReader reader, String idO) throws IOException {
        reader.beginObject();
        Object obj = new Object(idO);
        while (reader.hasNext()) {
            String name = reader.nextName();
            if (name.equals("name")) {
                obj.setName(reader.nextString());
            } else if (name.equals("brand")){
                obj.setBrand(reader.nextString());
            } else if (name.equals("year")){
                obj.setYear(reader.nextString());
            }else if (name.equals("timeFrame")){
                reader.beginArray();
                String tmp = "";
                while (reader.hasNext()) {
                    tmp = tmp + reader.nextString();
                    if(reader.hasNext()){
                        tmp = tmp + ", ";
                    }
                }
                reader.endArray();
                obj.setDecade(tmp);
            }else if (name.equals("description")){
                String test = reader.nextString();
                if (test.equals(""))
                {
                    obj.setDesc("Inconnu");
                }else{
                    obj.setDesc(test);
                }
            }else if (name.equals("technicalDetails")){
                reader.beginArray();
                String tmp = "";
                while (reader.hasNext()) {
                    tmp = tmp + reader.nextString();
                    if(reader.hasNext()){
                        tmp = tmp + ", ";
                    }
                }
                reader.endArray();
                obj.setDetails(tmp);
            }else if (name.equals("categories")){
                reader.beginArray();
                String tmp = "";
                while (reader.hasNext()) {
                    tmp = tmp + reader.nextString();
                    if(reader.hasNext()){
                        tmp = tmp + ", ";
                    }
                }
                reader.endArray();
                obj.setCat(tmp);
            } else {
                reader.skipValue();
            }
        }
        if (obj.getBrand() == null){
            obj.setBrand("Inconnu");
        }
        if (obj.getYear() == null){
            obj.setYear("Iconnu");
        }
        if (obj.getDetails() == null){
            obj.setDetails("Iconnu");
        }
        reader.endObject();
        //System.out.println(obj.getName() + " :: " + obj.getIdObject() + " :: " + obj.getBrand() + " :: " + obj.getYear() + " :: " + obj.getDeacade() + " :: " + obj.getDesc() + " :: " + obj.getDetails() + " :: " + obj.getCat());
        object.add(obj);
    }



}
