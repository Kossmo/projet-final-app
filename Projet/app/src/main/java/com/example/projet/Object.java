package com.example.projet;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import java.util.ArrayList;

public class Object {

    private long id;
    private String idObject;
    private String name;
    private String brand;
    private String year;
    private String decade;
    private String desc;
    private String details;
    private String cat;
    private byte[] img;

    public Object(String idObject){this.idObject = idObject;}

    public Object(long id, String idObject, String name, String brand, String year, String decade, String desc, String details, String cat, byte[] img){
        this.id = id;
        this.idObject = idObject;
        this.name = name;
        this.brand = brand;
        this.year = year;
        this.decade = decade;
        this.desc = desc;
        this.details = details;
        this.cat = cat;
        this.img = img;
    }

    public long getId(){return id;}

    public String getIdObject(){return idObject;}

    public String getName(){return name;}

    public String getBrand(){return brand;}

    public String getYear(){return year;}

    public String getDecade(){return decade;}

    public String getDesc(){return desc;}

    public String getDetails(){return details;}

    public String getCat(){return cat;}

    public  byte[] getImg(){return img;}

    public Bitmap getImgToBitmap(){
        return BitmapFactory.decodeByteArray(this.img, 0, this.img.length);
    }

    public void setId(long id){this.id = id;}

    public void setIdObject(String idObject){this.idObject = idObject;}

    public void setName(String name){this.name = name;}

    public void setBrand(String brand){this.brand = brand;}

    public void setYear(String year){this.year = year;}

    public void setDecade(String decade){this.decade = decade;}

    public void setDesc(String desc){this.desc = desc;}

    public void setDetails(String details){this.details = details;}

    public void setCat(String cat){this.cat = cat;}

    public void setImg(byte[] img) {this.img = img;}
}
