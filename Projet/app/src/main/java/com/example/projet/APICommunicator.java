package com.example.projet;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

public class APICommunicator extends AsyncTask<Void, Void, ArrayList<Object>> {

    private Context mContext;
    Activity activity;

    public APICommunicator(Context mContext, Activity activity){
        this.mContext = mContext;
        this.activity = activity;
    }

    @Override
    protected ArrayList<Object> doInBackground(Void... voids) {
        JSONResponseHandlerObjectId jsId = new JSONResponseHandlerObjectId();
        HttpURLConnection connection = null;
        try {
            URL url = new URL("https://demo-lia.univ-avignon.fr/cerimuseum/ids");
            connection = (HttpURLConnection) url.openConnection();
            InputStream input = connection.getInputStream();
            jsId.readJsonStream(input);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        ArrayList<String> objId = jsId.getObjectId();
        JSONResponseHandlerObject jsO = new JSONResponseHandlerObject();
        for (int i = 0; i < objId.size(); i++){
            HttpURLConnection connection2 = null;
            try {
                URL url2 = new URL("https://demo-lia.univ-avignon.fr/cerimuseum/items/" + objId.get(i));
                connection2 = (HttpURLConnection) url2.openConnection();
                InputStream input = connection2.getInputStream();
                jsO.readJsonStream(input, objId.get(i));
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        ArrayList<Object> obj = jsO.getObject();
        for (int i = 0; i < obj.size(); i++){
            HttpURLConnection connection3 = null;
            Bitmap bit = null;
            try {
                URL url3 = new URL("https://demo-lia.univ-avignon.fr/cerimuseum/items/" + obj.get(i).getIdObject() + "/thumbnail");
                connection3 = (HttpURLConnection) url3.openConnection();
                InputStream input = connection3.getInputStream();
                bit = BitmapFactory.decodeStream(input);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            final Bitmap finalBit = bit;
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bit.compress(Bitmap.CompressFormat.PNG, 0, stream);
            byte[] image = stream.toByteArray();
            obj.get(i).setImg(image);
        }

        return obj;
    }

    protected void onPostExecute(ArrayList<Object> object) {
        ObjectDbHelper SDB = new ObjectDbHelper(mContext);
        for (int i = 0; i < object.size(); i++){
            SDB.addObject(object.get(i));
        }
        Cursor cursor = SDB.fetchAllObject();
        while(!cursor.isAfterLast()) {
            ObjectAdapter.get(mContext, SDB, activity).getListe().add(ObjectDbHelper.cursorToObject(cursor));
            ObjectAdapter.get(mContext, SDB, activity).notifyDataSetChanged();
            cursor.moveToNext();
        }
        ObjectAdapter.get(mContext, SDB, activity).setObjectLFull(ObjectAdapter.get(mContext, SDB, activity).getListe());
    }

    /*
    public ArrayList<String> getObjectIdFromAPI(){
        JSONResponseHandlerObjectId jsId = new JSONResponseHandlerObjectId();
        HttpURLConnection connection = null;
        try {
            URL url = new URL("https://demo-lia.univ-avignon.fr/cerimuseum/ids");
            connection = (HttpURLConnection) url.openConnection();
            InputStream input = connection.getInputStream();
            jsId.readJsonStream(input);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        ArrayList<String> objId = jsId.getObjectId();
        return objId;
    }

    public ArrayList<Object> getObjectFromAPI(){
        JSONResponseHandlerObject jsO = new JSONResponseHandlerObject();
        ArrayList<String> objId = getObjectIdFromAPI();
        for (int i = 0; i < objId.size(); i++){
            HttpURLConnection connection = null;
            try {
                URL url = new URL("https://demo-lia.univ-avignon.fr/cerimuseum/items/" + objId.get(i));
                connection = (HttpURLConnection) url.openConnection();
                InputStream input = connection.getInputStream();
                jsO.readJsonStream(input, objId.get(i));
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        ArrayList<Object> obj = jsO.getObject();
        return obj;
    }*/
}
