package com.example.projet;

import android.util.JsonReader;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class JSONResponseHandlerObjectId {

    private static final String TAG = JSONResponseHandlerObjectId.class.getSimpleName();

    private ArrayList<String> objectId;

    public JSONResponseHandlerObjectId() {
        this.objectId = new ArrayList<>();
    }

    public ArrayList<String> getObjectId(){ return this.objectId;}

    public void readJsonStream(InputStream response) throws IOException {
        JsonReader reader = new JsonReader(new InputStreamReader(response, "UTF-8"));
        try {
            readObjects(reader);
        } finally {
            reader.close();
        }
    }

    public void readObjects(JsonReader reader) throws IOException {
        reader.beginArray();
        while (reader.hasNext()) {
            String name = reader.nextString();
            objectId.add(name);
        }
        reader.endArray();
    }

}
