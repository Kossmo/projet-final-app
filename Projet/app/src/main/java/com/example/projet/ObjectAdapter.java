package com.example.projet;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.Collection;

public class ObjectAdapter extends RecyclerView.Adapter<ObjectAdapter.ViewHolder> implements Filterable {

    private static final String TAG = "ObjectListAdapter";
    private Context aContext;
    private static ArrayList<Object> ObjectL = new ArrayList<>();
    private ArrayList<Object> ObjectLFull;
    private ObjectDbHelper ObjectDB;
    private static ObjectAdapter singleton;
    private Activity activity;

    public ObjectAdapter(Context context, ObjectDbHelper ODB, Activity activity){
        aContext = context;
        ObjectDB = ODB;
        this.activity = activity;
        Cursor cursor = ODB.fetchAllObject();
        while(!cursor.isAfterLast()) {
            ObjectL.add(ObjectDbHelper.cursorToObject(cursor));
            cursor.moveToNext();
        }
        ObjectLFull = new ArrayList<>(ObjectL);
    }

    public void setObjectLFull(ArrayList<Object> ObjectLFull){this.ObjectLFull = new ArrayList<>(ObjectLFull);}

    public static  ObjectAdapter get(Context context, ObjectDbHelper SDB, Activity activity){
        if(singleton == null) {
            singleton = new ObjectAdapter(context,SDB,activity);
        }
        return singleton;
    }

    public static ArrayList<Object> getListe(){
        return ObjectL;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_object, parent,false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        holder.name.setText(ObjectL.get(position).getName());
        holder.brand.setText(ObjectL.get(position).getBrand());
        holder.cat.setText(ObjectL.get(position).getCat());
        holder.img.setImageBitmap(ObjectL.get(position).getImgToBitmap());
        holder.pLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(aContext,ObjectActivity.class);
                intent.putExtra("position", position);
                aContext.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return ObjectL.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView name;
        TextView brand;
        TextView cat;
        ImageView img;
        RelativeLayout pLayout;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.name);
            brand = itemView.findViewById(R.id.brand);
            cat = itemView.findViewById(R.id.cat);
            pLayout = itemView.findViewById(R.id.pLayout);
            img = itemView.findViewById(R.id.img);
        }
    }

    @Override
    public Filter getFilter() {
        return filter;
    }

    private Filter filter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            ArrayList<Object> filteredL = new ArrayList<>();

            if(constraint == null || constraint.length() == 0){
                filteredL.addAll(ObjectLFull);
            } else {
                String pattern = constraint.toString().toLowerCase().trim();
                for (Object object : ObjectLFull){
                    if(object.getName().toLowerCase().contains(pattern) || object.getBrand().toLowerCase().contains(pattern) || object.getCat().toLowerCase().contains(pattern) || object.getYear().toLowerCase().contains(pattern) || object.getDecade().toLowerCase().contains(pattern) || object.getDetails().toLowerCase().contains(pattern) || object.getDesc().toLowerCase().contains(pattern)){
                        filteredL.add(object);
                    }
                }
            }
            FilterResults results = new FilterResults();
            results.values = filteredL;
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            ObjectL.clear();
            ObjectL.addAll((ArrayList) results.values);
            notifyDataSetChanged();
        }
    };
}