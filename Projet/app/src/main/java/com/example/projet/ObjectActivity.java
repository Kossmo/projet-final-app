package com.example.projet;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;

public class ObjectActivity extends AppCompatActivity {

    private TextView textObjectName, textObjectBrand, textObjectYear, textObjectDeacade, textObjectDesc, textObjectDetails, textObjectCat;
    private ImageView img;
    private Object object;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_object);

        int position = (int) getIntent().getExtras().get("position");
        object = ObjectAdapter.getListe().get(position);

        textObjectName = (TextView) findViewById(R.id.objectName);
        textObjectBrand = (TextView) findViewById(R.id.objectBrand);
        textObjectYear = (TextView) findViewById(R.id.objectYearV);
        textObjectDeacade = (TextView) findViewById(R.id.objectDecadeV);
        textObjectDesc = (TextView) findViewById(R.id.objectDescriptionV);
        textObjectDetails = (TextView) findViewById(R.id.objectDetailsV);
        textObjectCat = (TextView) findViewById(R.id.objectCatV);

        img = (ImageView) findViewById(R.id.objectImg);

        System.out.println(object.getIdObject());
        updateView();
    }

    private void updateView() {

        textObjectName.setText(object.getName());
        textObjectBrand.setText(object.getBrand());
        textObjectYear.setText(object.getYear());
        textObjectDeacade.setText(object.getDecade());
        textObjectDesc.setText(object.getDesc());
        textObjectDetails.setText(object.getDetails());
        textObjectCat.setText(object.getCat());
        img.setImageBitmap(object.getImgToBitmap());
    }
}
